/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import dao.HibernateUtil;
import dao.UploadFileDAO;
import java.util.List;
import model.UploadedFile;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Cruelkid
 */
public enum UploadedFileDAOImpl implements UploadFileDAO {

    INSTANCE;

    @Override
    public void save(UploadedFile uploadedFile) {
        Transaction tx = null;
        Session session = HibernateUtil.getSession();

        tx = session.beginTransaction();
        session.save(uploadedFile);
        tx.commit();
        session.close();
    }

    @Override
    public List<UploadedFile> fetchAll() {
        Session session = HibernateUtil.getSession();
        List<UploadedFile> list = session.createCriteria(UploadedFile.class).list();
        session.close();
        return list;
    }
    
    @Override
    public UploadedFile getUploadFileById(Long id) {

        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(UploadedFile.class, "UploadedFile").add(Restrictions.eq("id", id));
        UploadedFile uploadedFile = (UploadedFile)criteria.uniqueResult();
        session.close();
        return uploadedFile;
    }
    
    @Override
    public List<UploadedFile> read() {

        Session session = HibernateUtil.getSession();
        List<UploadedFile> result = (List<UploadedFile>)session.createQuery("from UploadedFile order by id").list();
        session.close();

        return result;
    }
    
}
