/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.UploadedFile;

/**
 *
 * @author Cruelkid
 */
public interface UploadFileDAO {
    
    void save(UploadedFile uploadedFile);

    List<UploadedFile> fetchAll();
    
    UploadedFile getUploadFileById(Long id);
    
    List<UploadedFile> read();
    
}
