package dao;

import model.User;

public interface UserDAO {

    void save(User user);

    User getUserByEmail(String email);

    User getUserById(Long id);

    void update(User user);

    void delete(User user);

    boolean isPasswordCorrect(String login, String pass);
}
