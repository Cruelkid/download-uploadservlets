package model;


import javax.persistence.*;
import org.hibernate.annotations.Type;
import org.hibernate.type.BlobType;

@Entity
@Table(name = "UPLOADED_FILES")
public class UploadedFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FILE_NAME", unique = true)
    private String fileName;
    @Column(name = "FILE_PATH")
    private String filePath;
    @Column(name = "DATA") //, length = 50000
    @Type(type = "org.hibernate.type.ImageType")
    private byte[] data;
//    @Lob
//    @Column(name = "DATA", columnDefinition = "MEDIUMBLOB")
//    private BlobType fileData;
//    private byte[] data;

    public UploadedFile() {
    }

    public UploadedFile(String fileName, String filePath, /*BlobType fileData*/byte[] data) {
        this.fileName = fileName;
        this.filePath = filePath;
//        this.fileData = fileData;
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
//    public BlobType fileData() {
//        return fileData;
//    }
//    
//    public void setFileData(BlobType fileData) {
//        this.fileData = fileData;
//    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
