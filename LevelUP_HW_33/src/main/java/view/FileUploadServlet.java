package view;

import dao.UploadFileDAO;
import dao.impl.UploadedFileDAOImpl;
import model.UploadedFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import static view.Constants.*;

@WebServlet(urlPatterns = {"/upload"})
@MultipartConfig(maxFileSize = 10 * 1024 * 1024)
public class FileUploadServlet extends HttpServlet {
    
    private UploadedFileDAOImpl uploadedFileDAO = UploadedFileDAOImpl.INSTANCE;

    private final static Logger LOGGER =
            Logger.getLogger(FileUploadServlet.class.getCanonicalName());

    private UploadFileDAO uploadFileDAO = UploadedFileDAOImpl.INSTANCE;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(UPLOAD_PAGE).forward(req, resp);
        
//        processUpload(req, resp);
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");

        processUpload(req, resp);
    }

    protected void processUpload(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             response.setContentType("text/html;charset=UTF-8");
             
             // Create path components to save the file
        final String path = /*System.getProperty("catalina.home") + */request.getParameter("destination");
        final Part filePart = request.getPart("file");

        if (filePart.getSize() >= 10*1024*1024) {
            final PrintWriter writer = response.getWriter();
            writer.println("Very big file. Max size: "+ 10*1024*1024 +" bytes");
        }
        else{

        final String fileName = getFileName(filePart);
        //final byte[] copyBytes;

        final PrintWriter writer = response.getWriter();
        try {
            File file = new File(path + File.separator + fileName);

            try (InputStream reader = filePart.getInputStream();
                 FileOutputStream fileWrite = new FileOutputStream(file)) {

                int read;
                byte[] buffer = new byte[(int) filePart.getSize()];
                byte[] bytes = new byte[(int) filePart.getSize()];

                //final byte[] bytes = new byte[10240000];
                //int k = 0;
                while ((read = reader.read(buffer)) != -1) {
                    fileWrite.write(buffer, 0, read);
//                    for (int i = 0; i < buffer.length; i++ )
//                         bytes[k*BUFFER_SIZE + i] = buffer[i];
//                    k++;
                 }
                bytes = buffer.clone();

                //copyBytes = bytes.clone();

                uploadedFileDAO.save(new UploadedFile(fileName, path, buffer));   // test
                // It works!
//                UploadedFileImpl fileForDB = UploadedFileImpl.INSTANCE;
//                fileForDB.save(new UploadedFile(fileName, path, buffer));
               }

            //UploadedFileImpl fileForDB = UploadedFileImpl.INSTANCE;
            //fileForDB.save(new UploadedFile(fileName, path, fileForDB.getBlobCreator(file));

            writer.println("New file " + fileName + " created at " + path);
            LOGGER.log(Level.INFO, "File {0} being uploaded to {1}",
                    new Object[]{fileName, path});
        } catch (FileNotFoundException fne) {
            writer.println("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent "
                    + "location.");
            writer.println("<br/> ERROR: " + fne.getMessage());

            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
                    new Object[]{fne.getMessage()});
        }
        catch (HibernateException hne) {

            writer.println("Smth happens in Hibernate");
            writer.println("<br/> ERROR: " + hne.getMessage());
            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
                    new Object[]{hne.getMessage()});
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
        }

//        // Create path components to save the file
//        final String path = /*System.getProperty("catalina.home"); +*/ request.getParameter("destination");
//        final Part filePart = request.getPart("file");
//        final String fileName = getFileName(filePart);
//
//        final PrintWriter writer = response.getWriter();
//        try {
//            File file = new File(path + File.separator + fileName);
//
//            try (InputStream reader = filePart.getInputStream();
//                 FileOutputStream fileWriter = new FileOutputStream(file)) {
//
//                int read;
//                final byte[] bytes = new byte[1024];
//                int startIndex = 0;
//
//                byte[] data = new byte[10 * 1024 * 1024];
//
//                while ((read = reader.read(bytes)) != -1) {
//                    fileWriter.write(bytes, 0, read);
//                    System.arraycopy(bytes, 0, data, startIndex, bytes.length);
//                    startIndex += bytes.length;
//                }
//
//                uploadFileDAO.save(new UploadedFile(fileName, path, data));
//            }
//            writer.println("New file " + fileName + " created at " + path);
//            LOGGER.log(Level.INFO, "File {0} being uploaded to {1}",
//                    new Object[]{fileName, path});
//            
//        } catch (FileNotFoundException fne) {
//            writer.println("You either did not specify a file to upload or are "
//                    + "trying to upload a file to a protected or nonexistent "
//                    + "location.");
//            writer.println("<br/> ERROR: " + fne.getMessage());
//
//            LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}",
//                    new Object[]{fne.getMessage()});
//        } finally {
//            if (writer != null) {
//                writer.close();
//            }
//        }
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}
