package view;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/welcome")
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        req.setAttribute("lessonNumber", "28");
//        req.setAttribute("group", "Java");

        req.getRequestDispatcher("jsp/welcome.jsp").forward(req, resp);
    }
}