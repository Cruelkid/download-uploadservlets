/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.UploadFileDAO;
import dao.impl.UploadedFileDAOImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import model.UploadedFile;
import static view.Constants.DOWNLOAD_PAGE;

/**
 *
 * @author cruelkid
 */
@WebServlet("/filelist")
public class FileListServlet extends HttpServlet {

    private UploadFileDAO uploadFileDAO = UploadedFileDAOImpl.INSTANCE;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<UploadedFile> list = uploadFileDAO.fetchAll();
        req.setAttribute("list", list);
        req.getRequestDispatcher(DOWNLOAD_PAGE).forward(req, resp);
    }
      
}

