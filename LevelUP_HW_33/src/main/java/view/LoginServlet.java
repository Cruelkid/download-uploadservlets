package view;

import service.UserService;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private UserService userService = UserServiceImpl.INSTANCE;

    private final static Logger LOGGER =
            Logger.getLogger(LoginServlet.class.getCanonicalName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String pass = req.getParameter("pass");

        LOGGER.info("User with login = " + login + " entered");

        if (userService.isPasswordCorrect(login, pass)) {
            LOGGER.info("User with login = " + login + " logged successfully");
            req.setAttribute("login", login);
            req.getRequestDispatcher("jsp/helloPage.jsp").forward(req, resp);
        } else {
            LOGGER.info("User with login = " + login + " not logged in. Password is not correct");
            req.getRequestDispatcher("jsp/error.jsp").forward(req, resp);
        }

    }
}
