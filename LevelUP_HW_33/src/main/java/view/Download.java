/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.UploadFileService;
import service.impl.UploadFileServiceImpl;

/**
 *
 * @author Cruelkid
 */
@WebServlet("/download")
public class Download extends HttpServlet {
    
    private UploadFileService uploadFileService = UploadFileServiceImpl.INSTANCE;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        
        processDownload(req, resp);

    }

    protected void processDownload(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");
        response = uploadFileService.downloadFileFromDatabase(response, Long.parseLong(param));

    }
    
}
