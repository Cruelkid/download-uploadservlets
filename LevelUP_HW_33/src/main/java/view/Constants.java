/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author Cruelkid
 */
public class Constants {

    private static final String JSP = ".jsp";

    private static final String DEFAULT_JSP_PATH = "/jsp";

    public static final String WELCOME_PAGE = DEFAULT_JSP_PATH + "/welcome" + JSP;

    public static final String HELLO_PAGE = DEFAULT_JSP_PATH + "/helloPage" + JSP;

    public static final String REGISTRATION_PAGE = DEFAULT_JSP_PATH + "/registration" + JSP;

    public static final String LOGIN_PAGE = DEFAULT_JSP_PATH + "/login" + JSP;

    public static final String UPLOAD_PAGE = DEFAULT_JSP_PATH + "/upload" + JSP;

    public static final String DOWNLOAD_PAGE = DEFAULT_JSP_PATH + "/download" + JSP;

    public static final String ERROR_PAGE = DEFAULT_JSP_PATH + "/error" + JSP;
    
}
