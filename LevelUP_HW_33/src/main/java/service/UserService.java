package service;

import model.User;

public interface UserService {

    boolean registerUser(User user);

    boolean isPasswordCorrect(String login, String pass);
    
    User getUserById(Long id);
    
}
