/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cruelkid
 */
public interface UploadFileService {
    
  public HttpServletResponse downloadFileFromDisk(HttpServletResponse response, Long id);
  
  public HttpServletResponse downloadFileFromDatabase(HttpServletResponse response, Long id);
    
}
