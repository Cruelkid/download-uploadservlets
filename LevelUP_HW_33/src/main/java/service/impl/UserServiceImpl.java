package service.impl;

import dao.UserDAO;
import dao.impl.UserDAOImpl;
import model.User;
import service.UserService;

public enum UserServiceImpl implements UserService {

    INSTANCE;

    private UserDAO userDAO = UserDAOImpl.INSTANCE;

    public boolean registerUser(User user) {
        if (userDAO.getUserByEmail(user.getEmail()) == null) {
            userDAO.save(user);
            System.out.println(String.format("User with email %s created", user.getEmail()));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isPasswordCorrect(String login, String pass) {
        return userDAO.isPasswordCorrect(login, pass);
    }
    
    @Override
    public User getUserById(Long id) {
        return userDAO.getUserById(id);
    }

}
