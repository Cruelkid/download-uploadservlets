/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import dao.UploadFileDAO;
import dao.impl.UploadedFileDAOImpl;
import java.io.*;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletResponse;
import model.UploadedFile;
import service.UploadFileService;

/**
 *
 * @author Cruelkid
 */
public enum UploadFileServiceImpl implements UploadFileService {

    INSTANCE;

    private UploadFileDAO uploadfileDAO = UploadedFileDAOImpl.INSTANCE;

    @Override
    public HttpServletResponse downloadFileFromDisk(HttpServletResponse response, Long id) {

        UploadedFile uploadedFile = uploadfileDAO.getUploadFileById(id);

        String shortName = uploadedFile.getFileName();
        response = downloadFile(response, uploadedFile.getFilePath() + File.separator + uploadedFile.getFileName(),
                String.valueOf(id)+shortName.substring(shortName.lastIndexOf("."), shortName.length()));

        return response;
    }

    @Override
    public HttpServletResponse downloadFileFromDatabase(HttpServletResponse response, Long id) {

        UploadedFile uploadedFile = uploadfileDAO.getUploadFileById(id);

        // Check parent's path
        File parent = new File(uploadedFile.getFilePath());

        // Check path and file name
        File newFile =  new File(((parent.exists())? parent.getPath() : System.getProperty("catalina.home"))
                + File.separator
                + ((uploadedFile.getFileName() == null)? "123" : uploadedFile.getFileName()));

        // Delete file on disk
        if (newFile.exists()) newFile.delete();

        // Create new file
        try (FileOutputStream fileWrite = new FileOutputStream(newFile)) {

            fileWrite.write(uploadedFile.getData());

            String shortName = uploadedFile.getFileName();
            response = downloadFile(response, newFile.getAbsolutePath(),
                    String.valueOf(id)+shortName.substring(shortName.lastIndexOf("."), shortName.length()));

        } catch (IOException ioe) {
            System.out.println("Troubles with save file from DB to disk");
        }

        return response;

    }

    private HttpServletResponse downloadFile(HttpServletResponse response,  String fullFileName, String newFileName){

        //String getFile = "c://apache-tomcat-7.0.70/repository/ЗР-020190.png";
        if (fullFileName != null) {

            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Type","application/octet-stream; charset=UTF-8");

            try {
                //http://vova-sukhov.livejournal.com/1488.html  - Кириллица в имени файла при загрузке файла с сервера
                String URLEncodedFileName = null;
                String shortname = fullFileName.substring(fullFileName.lastIndexOf("\\") + 1, fullFileName.length());
                URLEncodedFileName = URLEncoder.encode(shortname, "UTF-8");
                String ResultFileName = URLEncodedFileName.replace('+', ' ');
                response.setHeader("Content-Disposition", "filename=\""+ResultFileName+"\";");
            } catch (UnsupportedEncodingException e) {
                String shortname = newFileName;
                response.setHeader("Content-Disposition",
                "charset=UTF-8; filename=\"" + shortname + "\";");
            }

            response.setCharacterEncoding("UTF-8");

            try {
                BufferedInputStream in = new
                        BufferedInputStream(new FileInputStream(fullFileName));
                BufferedOutputStream binout = new
                        BufferedOutputStream(response.getOutputStream());
                int ch;
                while ((ch = in.read()) != -1)
                    binout.write(ch);

                binout.close();
                in.close();
            } catch (IOException ioe) {
                System.out.println("Unable to get access");
            }
        }

        return response;

    }
    
}
