<%-- 
    Document   : login
    Created on : Aug 26, 2016, 4:21:24 PM
    Author     : (s)AINT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/login.css" title="style" />
    </head>
    <body>
    <div class="container">
        <form class="form-login" action="${pageContext.request.contextPath}/login" method="post">

            <h2 class="form-signin-heading">Please sign in:</h2>

            <input class="form-control" id="inputLogin" type="text" placeholder="Enter email" name="login"
                   required>

            <input class="form-control" id="inputPassword" type="password" placeholder="Enter password" name="pass"
                   required>

            <div class="checkbox" style="display: none">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>

            <br><button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            <button id="welcome" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/welcome'">Go to main page</button>

        </form>
    </div>
    </body>
</html>
