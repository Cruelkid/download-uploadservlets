<%-- 
    Document   : upload
    Created on : Aug 26, 2016, 4:21:30 PM
    Author     : (s)AINT
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

    <head>
      <title>File Upload</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link href="<c:url value="css/upload.css"/>" rel="stylesheet" type="text/css">
    </head>

    <body>

    <form method="POST" action="${pageContext.request.contextPath}/upload" enctype="multipart/form-data" >

      File:
      <br><input type="file" name="file" id="file" /> <br/>

      Destination:
      <input class = "dest" type="text" value="/home/cruelkid/testFolder" name="destination"/></br>

      <input type="submit" value="Upload" name="upload" id="upload" />
      
    </form>
    
    <button id="welcome" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/welcome'">Go to main page</button>

    </body>

</html>
