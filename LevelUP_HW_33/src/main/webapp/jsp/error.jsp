<%-- 
    Document   : error
    Created on : Aug 26, 2016, 4:21:48 PM
    Author     : (s)AINT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/error.css" title="style" />
    </head>
    <body>

    <h1>${error_message}</h1>

    <form action="${pageContext.request.contextPath}/welcome" method="get">
        <button type="submit">Go to main page</button>
    </form>

    </body>
</html>
