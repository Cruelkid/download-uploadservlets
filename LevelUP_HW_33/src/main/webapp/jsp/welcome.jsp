<%-- 
    Document   : welcome
    Created on : Aug 26, 2016, 4:21:56 PM
    Author     : (s)AINT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/welcome.css" title="style" />
        </head>
    <body>

    <div class="container form-welcome">

        <h2 class="form-welcome-heading">Welcome!</h2>

        <button id="login" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/login'">Login</button>

        <button id="register" class="btn btn-success" onclick="document.location.href='${pageContext.request.contextPath}/register'">Register</button>
        
        <button id="upload" class="btn btn-success" onclick="document.location.href='${pageContext.request.contextPath}/upload'">Upload</button>
        
        <button id="download" class="btn btn-success" onclick="document.location.href='${pageContext.request.contextPath}/filelist'">Download</button>

    </div>

    </body>
</html>
