<%-- 
    Document   : helloPage
    Created on : Sep 17, 2016, 8:28:36 PM
    Author     : cruelkid
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/helloPage.css" title="style" />
        <title></title>
    </head>
    
    <body>
        
        <div class="container form-welcome">
            
            <h2>Hello, ${login}!</h2>
                       
            <button id="welcome" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/welcome'">Go to main page</button>
            
        </div>
            
    </body>
</html>
