<%-- 
    Document   : download
    Created on : Sep 25, 2016, 12:35:31 AM
    Author     : cruelkid
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

    <head>
        <title>Download</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="css/download.css"/>" rel="stylesheet" type="text/css">
    </head>

    <body>

        <button id="welcome" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/welcome'">Go to main page</button>
        
        <button id="upload" class="btn btn-success" onclick="document.location.href='${pageContext.request.contextPath}/upload'">Upload</button>
        
        <table>
            <tr>
                <td>ID</td>
                <td>File</td>
            </tr>
            <c:forEach var="file" items="${list}">
                <tr>
                    <th>${file.id}</th>
                    <th>
                        
                        <a href="${pageContext.servletContext.contextPath}/download?param=${file.id}">${file.fileName}</a>

                    </th>
                </tr>
            </c:forEach>
        </table>
        

</html>

        
        <!--<script>
            window.onload  = function() {
                console.log("load table");
        
                // Find a <table> element with id="myTable":
                var table = document.getElementById("uploaded_files_table");
        
                var index = 1;
        
        <%--<c:forEach var="file" items="${list}">--%>

//            // Create an empty <tr> element and add it to the 1st position of the table:
//            var row = table.insertRow(index);
//
//            // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
//            var fileName = row.insertCell(0);
//            // Add some text to the new cells:
//            fileName.innerHTML = '${file.fileName}';
//
//            var filePath = row.insertCell(1);
//            filePath.innerHTML = '${file.filePath}';
//
//            var btn = row.insertCell(2);
//            btn.innerHTML = '<input type="button" data="${file.filePath}" value="Download">';
//
//            index++;

        <form action="url" method="post">

            <input type="submit" >

        </form>

        <%--</c:forEach>--%>
    }
</script>-->

        <!--<table id="uploaded_files_table">
            <tr><td>File name</td><td>FilePath</td><td>Action</td></tr>
        </table>-->


    </body>