<%-- 
    Document   : registration
    Created on : Aug 26, 2016, 4:21:38 PM
    Author     : (s)AINT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Registration</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/registration.css" title="style" />
    </head>
    <body>
    <form class="form-register" action="${pageContext.request.contextPath}/register" method="post">

        <div>
            <label><b>Email</b></label>
            <input type="text" placeholder="Enter Email" name="email" required>
        </div>
        <div>
            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required>
        </div>
        <div>
            <label><b>Nickname</b></label>
            <input type="text" placeholder="Enter Nickname" name="nickName">
        </div>
        <div>
            <label><b>First Name</b></label>
            <input type="text" placeholder="Enter First Name" name="firstName">
        </div>
        <div>
            <label><b>Last Name</b></label>
            <input type="text" placeholder="Enter Last Name" name="lastName">
        </div>
        <button type="submit">Register</button>
        <button id="welcome" class="btn btn-primary" onclick="document.location.href='${pageContext.request.contextPath}/welcome'">Go to main page</button>

    </form>
    </body>
</html>
